#include <iostream>
#include <assert.h>

using namespace std;

// Один элемент списка
struct ListElement {
  int value;
  ListElement *next;
};

struct List {
  ListElement *root;
  int ElementsCount;
  // Конструктор
  List(){
    root = NULL;
    ElementsCount = 0;
  }
  // Деструктор (очистка памяти)
  ~List(){
    while(root != NULL){
      ListElement *cur = root->next;
      delete root;
      root = cur;
    }
  }
  // Показать список
  void show(){
    ListElement *cur = root;
    while(cur != NULL){
      // cur->value <-> (*cur).value
      cout << cur->value << endl;
      cur = cur->next;
    }
    cout << endl;
  }
  // Добавить элемент в начало
  void addToBegin(int value){
    ListElement *newElement = new ListElement;
    newElement->value = value;
    // Подвешиваем к новому элементу старый список
    newElement->next = root;
    // Теперь root должен ссылаться на новый элемент
    root = newElement;
    ++ElementsCount;
  }
  // Добавить элемент в конец
  void addToEnd(int value){
    if(root == NULL)
    {
        addToBegin(value);
        return;
    }
    // Ищем последний элемент
    ListElement *cur = root;
    while(cur->next != NULL){
      cur = cur->next;
    }
    // Убеждаемся в том, что это последний элемент
    // списка
    assert(cur->next == NULL);

    // Заводим новый элемент
    ListElement *newElement = new ListElement;
    newElement->value = value;
    newElement->next = NULL;
    // Подвешиваем новый элемент в конец списка
    cur->next = newElement;
    ++ElementsCount;
  }

  void DeleteElement(int NoElement)
  {
    if (root == NULL) return;
    ListElement *Cur = root;
    ListElement *PrevElement = NULL;
    int CurNo = 0;
    while ((Cur->next != NULL) & (CurNo != NoElement))
    {
      PrevElement = Cur;
      Cur = Cur->next;
      ++CurNo;
    }
    if (CurNo != NoElement) return;

    if (PrevElement == NULL)
        root = Cur->next;
    else
        PrevElement->next = Cur->next;
    delete Cur;
    --ElementsCount;
  }
};

int main()
{
  List l,list2;

  l.addToBegin(2);
  l.addToBegin(10);
  l.addToBegin(-1);
  l.addToEnd(33);
  l.addToEnd(35);
  l.show();
  l.DeleteElement(0);
  l.DeleteElement(l.ElementsCount - 1);

  setlocale(LC_ALL,"Russian");
  wcout << L"Удаляем первый и последний\n";
  l.show();

  wcout << L"Удаляем второй\n";
  l.DeleteElement(1);
  l.show();
  return 0;
}
