#include "calc.h"
#include "model.h"
#include <QtWidgets/QApplication>
// #include <QObject>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	calc w;
	model CalcModel;
	QObject::connect(w.ui.InputNumbers, SIGNAL( textChanged( const QString & ) ),
            &CalcModel, SLOT( SetIOString( const QString &) ));
	QObject::connect(&CalcModel, SIGNAL( valueIOStringChanged(const QString & ) ),
            w.ui.InputNumbers, SLOT( setText(const QString &) ));
	QObject::connect(w.ui.pushButton, SIGNAL ( clicked(bool) ),
			&CalcModel, SLOT( Solve() ) );
	QObject::connect(w.ui.InputNumbers, SIGNAL ( returnPressed() ),
			w.ui.pushButton, SLOT( click() ) );
	CalcModel.SetIOString("24+93");
	w.show();
	return a.exec();
}
