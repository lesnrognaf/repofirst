/********************************************************************************
** Form generated from reading UI file 'calc.ui'
**
** Created by: Qt User Interface Compiler version 5.3.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CALC_H
#define UI_CALC_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_calcClass
{
public:
    QWidget *centralWidget;
    QLineEdit *InputNumbers;
    QLabel *label;
    QPushButton *pushButton;

    void setupUi(QMainWindow *calcClass)
    {
        if (calcClass->objectName().isEmpty())
            calcClass->setObjectName(QStringLiteral("calcClass"));
        calcClass->resize(391, 92);
        centralWidget = new QWidget(calcClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        InputNumbers = new QLineEdit(centralWidget);
        InputNumbers->setObjectName(QStringLiteral("InputNumbers"));
        InputNumbers->setGeometry(QRect(20, 10, 311, 41));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(200, 60, 181, 20));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(340, 10, 41, 41));
        pushButton->setAutoDefault(false);
        pushButton->setDefault(true);
        calcClass->setCentralWidget(centralWidget);

        retranslateUi(calcClass);

        QMetaObject::connectSlotsByName(calcClass);
    } // setupUi

    void retranslateUi(QMainWindow *calcClass)
    {
        calcClass->setWindowTitle(QApplication::translate("calcClass", "calc", 0));
        label->setText(QApplication::translate("calcClass", "\320\234\320\276\320\266\320\275\320\276 \320\275\320\260\320\266\320\260\321\202\321\214 Enter \320\264\320\273\321\217 \320\277\320\276\320\264\321\201\321\207\320\265\321\202\320\260", 0));
        pushButton->setText(QApplication::translate("calcClass", "=", 0));
    } // retranslateUi

};

namespace Ui {
    class calcClass: public Ui_calcClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CALC_H
