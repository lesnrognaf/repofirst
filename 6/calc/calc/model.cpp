#include "model.h"
#include <QRegExp>
#include <QList>
#include <QStringList>

void model::SetIOString (const QString & text)
{
	IOString = text;
	emit valueIOStringChanged(text);
}

void model::Solve()
{
	QList<double> DightList;
	QStringList Symbols;

	QRegExp rx("[-+]?(?:\\b[0-9]+(?:\\.[0-9]*)?|\\.[0-9]+\\b)(?:[eE][-+]?[0-9]+\\b)?");
	// QString str = "Offsets: 12 14 99 231 7";
	int pos = 0;
	while ((pos = rx.indexIn(IOString, pos)) != -1)
	{
		DightList << rx.cap(0).toDouble();
		pos += rx.matchedLength();
	}

	//TODO приоритет операций
	pos = 0;
	//QRegExp simbol("[\x002B\x002A\x002D\x002F]+");
	QRegExp simbol("[\\+\\-\\*\\/]+");
	while ((pos = simbol.indexIn(IOString, pos)) != -1)
	{
		Symbols << simbol.cap(0);
		pos += simbol.matchedLength();
	}

	if ((DightList.count() < 2) | (Symbols.count() < 1))
		return;
	
	if (Symbols[0] == "+") SetIOString(QString::number(DightList[0] + DightList[1]));
	if (Symbols[0] == "-") SetIOString(QString::number(DightList[0] - DightList[1]));
	if (Symbols[0] == "*") SetIOString(QString::number(DightList[0] * DightList[1]));
	if (Symbols[0] == "/") SetIOString(QString::number(DightList[0] / DightList[1]));
}
