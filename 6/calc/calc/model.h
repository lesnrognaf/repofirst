#include <string>
#include <QObject>

using namespace std;

class model : public QObject
{
	Q_OBJECT
//public:
public slots:
	void SetIOString(const QString & text);
	void Solve();
signals:
	void valueIOStringChanged(const QString & text);
private:
	QString IOString;
};