#include <map>
#include <iostream>
#include <iterator>
#include <fstream>
#include <string>

using namespace std;

namespace std {
    istream &operator >> (istream &is, pair<string, string> &p) {
        return is >> p.first >> p.second;
    }
}

int main()
{
    setlocale(LC_ALL, "Russian");
    ifstream in("voc.txt");

    map<string, string> data = map<string, string> (istream_iterator<pair<string, string> >(in),
               istream_iterator<pair<string, string> >());
    string EngWord;
    while (true)
    {
        cout << "Input english word: ";
        getline(cin, EngWord);
        cout << data[EngWord] << endl;
    }

    return 0;
}
